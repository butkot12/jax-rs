package ua.danit;

import com.sun.org.apache.bcel.internal.generic.Type;
import ua.danit.signature.Signature;
import utils.Reflections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static utils.Reflections.getMethods;

public class DispatcherServlet extends HttpServlet {

    Map<String, Signature> resources = new HashMap<>();

    public DispatcherServlet(Class<?>... classes) {

        for (Class<?> aClass : classes) {

            for (Method method : getMethods(aClass)) {
                if (method.isAnnotationPresent(Path.class)) {
                    Signature signature = getSignature(method, aClass);
                    resources.put(signature.getUrl(), signature);
                }
            }
        }
    }

    private Signature getSignature(Method method, Class<?> aClass) {
        Path path = (Path) Reflections.getAnnotation(method, Path.class);
        String uri = path.value();
        return new Signature(method, uri, aClass);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        Signature signature = resources.get(uri);
        String result = signature.invoke();
        resp.getWriter().write(result);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        Signature signature = resources.get(uri);
        String result = signature.invoke();
        resp.getWriter().write(result);
    }

}