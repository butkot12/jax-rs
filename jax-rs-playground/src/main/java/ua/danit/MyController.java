package ua.danit;

import javax.ws.rs.GET;
import javax.ws.rs.Path;


public class MyController {

    @Path("/my/custom/url")
    @GET
    public String getWeather() {
        return "The weather is cool";
    }

    @Path("/flights")
    @GET
    public String getFlights() {
        return "Please don't fly today";
    }

    @Path("/tickets")
    public String getTickets() {
        return "No ticket available.";
    }
}
