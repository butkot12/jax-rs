package ua.danit.signature;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Signature {

    String url;
    private Class<?> clazz;
    private Method method;

    public Signature(Method method, String url, Class<?> aClass) {
        this.method = method;
        this.url = url;
        clazz = aClass;
    }

    public String getUrl() {
        return url;
    }

    public Method getMethod() {
        return method;
    }

    public String invoke() {
        try {
            return (String)method.invoke(clazz.newInstance(), null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return "";
    }
}
