package utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;


/**
 * Created  15.12.2017.
 */
public class Reflections {

    public static List <Method> getMethods ( Class clazz ) {
        if ( clazz == null ) {
            return null;
        }
        return Arrays.asList(clazz.getMethods());
    }


    public static Annotation getAnnotation ( Method method ,Class annotationClass) {
        if ( method == null  ) {
            return null;
        }
        return method.getAnnotation(annotationClass);
    }
}
